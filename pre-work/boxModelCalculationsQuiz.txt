In this exercise, you will apply the equations for calculating heights and widths as discussed in the Box Model lesson.

The sample element you are working with is:

#div1 {
height: 150px;
width: 400px;
margin: 20px;
border: 1px solid red;
padding: 10px;
}

For this object, calculate:

TOTAL HEIGHT 
Formula: margin-top + border-top + padding-top + (height of the content) + padding-bottom + border-bottom + margin-bottom

SOLUTION: 20 + 1 + 10 + 150 + 10 + 1 + 20 = 212px 


TOTAL WIDTH
Formula: margin-left + border-left + padding-left + (width of the content) + padding-right + border-right + margin-right

SOLUTION: 20 + 1 + 10 + 400 + 10 + 1 + 20 = 462px 

BROWSER CALCULATED HEIGHT
Formula: border-top + padding-top + (height of the content) + padding-bottom + border-bottom

SOLUTION: 1 + 10 + 150 + 10 + 1 = 172px 

BROWSER CALCULATED WIDTH
Formula: border-left + padding-left + (width of the content) + padding-right + border-right

SOLUTION: 1 + 10 + 400 + 10 + 1 = 422px 









